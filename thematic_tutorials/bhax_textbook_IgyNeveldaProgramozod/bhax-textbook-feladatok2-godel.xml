<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Gödel!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
    	 <title>Gengszterek</title>
    	 <para>
    	 	Gengszterek rendezése lambdával a Robotautó Világbajnokságban
<link xlink:href="https://youtu.be/DL6iQwPx1Yw">https://youtu.be/DL6iQwPx1Yw</link> (8:05-től)
    	 </para>
    	 <para>
    	 	Megoldás forrása: <link xlink:href="https://github.com/nbatfai/robocar-emulator">robocar-emulator</link>, <link xlink:href="https://en.cppreference.com/w/cpp/language/lambda">cppreference</link>, <link xlink:href="https://riptutorial.com/cplusplus/example/5940/sorting-sequence-containers-using-lambda-expressions--cplusplus11-">riptutorial</link>
    	 </para>
    	 <para>
    	 	Megoldás videó: <link xlink:href="https://youtu.be/DL6iQwPx1Yw">https://youtu.be/DL6iQwPx1Yw</link>
    	 </para>
    	 <para>
    	 	A lambda kifejezések a C++ 11 részeként kerültek a nyelvbe. A <link xlink:href="https://en.cppreference.com/w/cpp/language/lambda">cppreference.com</link> alapján 4 formában használhatók, ezek közül a példánkban a harmadik alak látható.
    	 </para>
    	 <para>
    	 	Az ilyen módon megadott kifejezések tulajdonképp egy névtelen függvényt jelentenek. A szögletes zárójelek jelzik, hogy ilyen módon kezelendő kifejezés következik. Ezeket kerek zárójelek követik, ide kerülnek a paraméterek, míg a kapcsos zárójelek között kell elhelyezni azokat a sorokat, amiket normál esetben a függvényünkbe írnánk.
    	 </para>
    	 <para>
    	 	A standard library sort függvényének egy ilyet megadva tudjuk rendezni a gengsztereket.
    	 	Ezekben az esetekben a névtelen függvény visszatérési típusa automatikusan kerül meghatározásra.
    	 </para>
    	 <para>
    	 	Azért hasznos, mert saját objektumokat is tudunk így rendezni, amikre standard függvény nincs. A sort végére a gangsters vektorban a rendőrhöz legközelebb lévő gengszter lesz első helyen, tehát távolság szerint növekvő a sorrend.
    	 </para>
    	 <programlisting language="c++"><![CDATA[std::sort ( gangsters.begin(), gangsters.end(), [this, cop] ( Gangster x, Gangster y )
  {
    return dst ( cop, x.to ) < dst ( cop, y.to );
  } );]]></programlisting>
  		<para>
  			A <link xlink:href="https://riptutorial.com/cplusplus/example/5940/sorting-sequence-containers-using-lambda-expressions--cplusplus11-">riptutorial.com</link> weboldalon remek példa található egy másik gyakorlati használatra. A vector vektort ugyan ezzel a megoldással rendezzük:
  		</para>
  		<programlisting language="c++"><![CDATA[// Include sequence containers
#include <vector>
#include <deque>
#include <list>
#include <array>
#include <forward_list>

// Include sorting algorithm
#include <algorithm>

class Base {
 public:

    // Constructor that set variable to the value of v
    Base(int v): variable(v) {
    }
    
    int variable;
};


int main() {
    // Create 2 elements to sort
    Base a(10);
    Base b(5);
    
    // We're using C++11, so let's use initializer lists to insert items.
    std::vector <Base> vector = {a, b};
    std::deque <Base> deque = {a, b};
    std::list <Base> list = {a, b};
    std::array <Base, 2> array = {a, b};
    std::forward_list<Base> flist = {a, b};
    
    // We can sort data using an inline lambda expression
    std::sort(std::begin(vector), std::end(vector),
      [](const Base &a, const Base &b) { return a.variable < b.variable;});

    // We can also pass a lambda object as the comparator
    // and reuse the lambda multiple times
    auto compare = [](const Base &a, const Base &b) {
                     return a.variable < b.variable;};
    std::sort(std::begin(deque), std::end(deque), compare);
    std::sort(std::begin(array), std::end(array), compare);
    list.sort(compare);
    flist.sort(compare);

    return 0;
}]]></programlisting>
    </section>

    <section>
    	 <title>C++11 Custom Allocator</title>
    	 <para>
    	 	<link xlink:href="https://prezi.com/jvvbytkwgsxj/high-level-programming-languages-2-c11-allocators/">https://prezi.com/jvvbytkwgsxj/high-level-programming-languages-2-c11-allocators/</link> a
CustomAlloc-os példa, lásd C forrást az UDPROG repóban!
    	 </para>
    	 <para>
    	 	Megoldás forrása: <link xlink:href="https://howardhinnant.github.io/allocator_boilerplate.html">https://howardhinnant.github.io/allocator_boilerplate.html</link>
    	 </para>
    	 <para>
    	 	Megoldás videó: <link xlink:href="https://youtu.be/FVSicb0wD2c">https://youtu.be/FVSicb0wD2c</link>
    	 </para>
    	 <para>
    	 	A standard library tartalmaz egy allocator elemet is, ezzel valósítjuk meg a saját allokátort.
    	 </para>
    	 <para>
    	 	Template osztályt hozunk létre, így különböző típusoknak is tudunk majd foglalni memóriát. Foglaláskor az allocate függvény fut le. Ez megnézi, hogy a gépünkön mekkora az egyes típusok mérete, és annak megfelelően foglal nekik helyet.
    	 </para>
    	 <programlisting language="c++"><![CDATA[#include <iostream>
#include <string>
#include <vector>

template <class T>
class allocator
{
public:
    using value_type = T;

    value_type*
    allocate(std::size_t n)
    {
        std::cout <<"A " << n << " of " << n*sizeof(value_type) << std::endl;
        return static_cast<value_type*>(::operator new (n*sizeof(value_type)));
    }]]></programlisting>
    	<para>
    		A memória felszabadításánál is hasonló történik: az alapértelmezett méreteknek megfelelően szabadítjuk fel a memóriát, amit előzőleg foglaltunk (viszont vegyük észre, hogy ez void - nincs visszatérési érték).
    	</para>
    	<programlisting language="c++"><![CDATA[void
    deallocate(value_type* p, std::size_t n) noexcept
    {
        std::cout <<"D " << n << " of " << n*sizeof(value_type) << std::endl;
        ::operator delete(p);
    }
};]]></programlisting>
		<para>
			A main-ben létrehozunk egy int-eket tartalmazó vektort úgy, hogy az allokátorunk foglaljon majd neki memóriát. Futtatáskor láthatjuk, hogy a 2 foglalandó elemnek valóban megtörténik a foglalás, majd a törlésük is.
		</para>
		<para>
          <figure>
            <title>C++ Custom Allocator</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/cpp_custom_alloc.png"/>
                </imageobject>
                <textobject>
                    <phrase>C++ Custom Allocator</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
    </section>

    <section>
    	 <title>STL map érték szerinti rendezése</title>
    	 <para>
    	 	Például: <link xlink:href="https://github.com/nbatfai/future/blob/master/cs/F9F2/fenykard.cpp#L180">https://github.com/nbatfai/future/blob/master/cs/F9F2/fenykard.cpp#L180</link>
    	 </para>
    	 <para>
    	 	Megoldás forrása: <link xlink:href="https://github.com/nbatfai/future/blob/master/cs/F9F2/fenykard.cpp#L180">fenykard.cpp</link>
    	 </para>
    	 <para>
    	 	Ebben a feladatban is (mint a gengszterekben) lambda alapú rendezést használunk. A sort_map függvény végzi a rendezést, ezt vektorba teszi meg név-érték párokkal. Paraméterként egy std::map referenciát kap, ebből jönnek majd létre a párok. Létrehozzuk a vektort, és az ezt követő ciklussal a map-ből név-érték párokat képzünk, majd ezeket hozzáadjuk a vektorhoz.
    	 </para>
    	 <programlisting language="c++"><![CDATA[std::vector<std::pair<std::string, int>> sort_map ( std::map <std::string, int> &rank )
{
        std::vector<std::pair<std::string, int>> ordered;

        for ( auto & i : rank ) {
                if ( i.second ) {
                        std::pair<std::string, int> p {i.first, i.second};
                        ordered.push_back ( p );
                }
        }]]></programlisting>
        <para>
        	A párokkal feltöltött vektorra már alkalmazható a lambdás kifejezéssel hívott standard sort függvény, ami az első feladathoz hasonlóan rendezi azt (bővebben ott).
        	A rendezés után a függvény ezt a rendezett vektort adja vissza.
        </para>
        <programlisting language="c++"><![CDATA[std::sort (
                std::begin ( ordered ), std::end ( ordered ),
        [ = ] ( auto && p1, auto && p2 ) {
                return p1.second > p2.second;
        }
        );

        return ordered;
}]]></programlisting>
    </section>

    <section>
    	 <title>Alternatív Tabella rendezése</title>
    	 <para>
    	 	Mutassuk be a <link xlink:href="https://progpater.blog.hu/2011/03/11/alternativ_tabella">https://progpater.blog.hu/2011/03/11/alternativ_tabella</link> a programban a java.lang
Interface Comparable&lt;T&gt; szerepét!
    	 </para>
    	 <para>
    	 	Megoldás forrása: <link xlink:href="https://hu.wikipedia.org/wiki/Alternat%C3%ADv_tabella">Wikipédia</link>, <link xlink:href="https://progpater.blog.hu/2011/03/11/alternativ_tabella">https://progpater.blog.hu/2011/03/11/alternativ_tabella</link>
    	 </para>
    	 <para>
    	 	Megoldás videó: <link xlink:href="https://youtu.be/_wUxir685OA">https://youtu.be/_wUxir685OA</link>
    	 </para>
    	 <para>
    	 	Az alternatív tabella a labdarúgó bajnokságokhoz készített olyan sorrend, ami nem a megszokott mechanikus módon (amikor a győzelem 3 , a döntetlen 1 pontot ér) számolja a csapatok sorrendjét, hanem megpróbálja figyelembe venni azt, hogy egy adott csapat éppen melyik másik adott csapattal szemben érte el az adott eredményt.
    	 </para>
    	 <para>
          <figure>
            <title>Példa alternatív tabellára</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/tab_example.png"/>
                </imageobject>
                <textobject>
                    <phrase>Példa alternatív tabellára</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
    	 <para>
    	 	Az eredményeket a Wiki2Matrix osztályban lévő kereszt nevű mátrix fogja tartalmazni.
    	 	Ezzel fordítva és futtatva a programot, egy kevésbé szép eredményt kapunk, amit be kell illeszteni az AlternatívTabella L mátrixába.
    	 </para>
    	 <para>
          <figure>
            <title>Wiki2Matrix kimenete</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/tab_wiki_console_1.png"/>
                </imageobject>
                <textobject>
                    <phrase>Wiki2Matrix kimenete</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
          <figure>
            <title>Wiki2Matrix kimenete</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/tab_wiki_console_2.png"/>
                </imageobject>
                <textobject>
                    <phrase>Wiki2Matrix kimenete</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
          <figure>
            <title>L mátrix</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/tab_l.png"/>
                </imageobject>
                <textobject>
                    <phrase>L mátrix</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
        	Ha így fordítjuk és futtatjuk az AlternativTabella.java fájlt, akkor megkapjuk a csapatok ilyen módon rendezett listáját, és néhány helyezést meghatározó adatot.
        </para>
        <para>
          <figure>
            <title>AlternativTabella kimenete</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/tab_console_1.png"/>
                </imageobject>
                <textobject>
                    <phrase>AlternativTabella kimenete</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
          <figure>
            <title>AlternativTabella kimenete</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/tab_console_2.png"/>
                </imageobject>
                <textobject>
                    <phrase>AlternativTabella kimenete</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
          <figure>
            <title>AlternativTabella kimenete</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/tab_console_3.png"/>
                </imageobject>
                <textobject>
                    <phrase>AlternativTabella kimenete</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
          <figure>
            <title>AlternativTabella kimenete</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/tab_console_4.png"/>
                </imageobject>
                <textobject>
                    <phrase>AlternativTabella kimenete</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
        	A Comparable interfész használata teszi lehetővé azt, hogy az alternatív tabella értékeit össze tudjuk hasonlítani egymással. Implementálásánál létrehozunk egy compareTo függvényt is, 
        	ami egy Csapat példányt kap, értékeit összeveti:
        </para>
        <itemizedlist>
        	<listitem>
        		<para>
        			Ha az aktuális érték kisebb, mint a vizsgált elemé, akkor 1-et,
        		</para>
        	</listitem>
        	<listitem>
        		<para>
        			ha az aktuális érték nagyobb, mint a vizsgált elemé, akkor -1-et,
        		</para>
        	</listitem>
        	<listitem>
        		<para>
        			egyébként pedig 0-t.
        		</para>
        	</listitem>
        </itemizedlist>
    </section>

    <section>
    	 <title>GIMP Scheme hack</title>
    	 <para>
    	 	Ha az előző félévben nem dolgoztad fel a témát (például a mandalás vagy a króm szöveges
dobozosat) akkor itt az alkalom!
    	 </para>
    	 <para>
    	 	<command>Króm</command>
    	 </para>
        <para>
          <figure>
            <title>Króm effekt</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/gimp_chrome.png"/>
                </imageobject>
                <textobject>
                    <phrase>Króm effekt</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/OKdAkI_c7Sc">https://youtu.be/OKdAkI_c7Sc</link>,
            Saját videó: <link xlink:href="https://www.youtube.com/watch?v=9OAYVAivkrw">https://www.youtube.com/watch?v=9OAYVAivkrw</link>      
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/GIMP_Lisp/Chrome">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/GIMP_Lisp/Chrome</link>               
        </para>
        <para>
            A color-curve függvényben megadjuk egy 8 elemű tömb segítségével a színeket. Ezt követően megadjuk a betűk méretét (1), a kapott értékeket listába mentjük.
        </para>
        <programlisting><![CDATA[
(define (color-curve)
    (let* (
        (tomb (cons-array 8 'byte))
    )
        (aset tomb 0 0)
        (aset tomb 1 0)
        (aset tomb 2 50)
        (aset tomb 3 190)
        (aset tomb 4 110)
        (aset tomb 5 20)
        (aset tomb 6 200)
        (aset tomb 7 190)
    tomb)    
)

;(color-curve)

(define (elem x lista)

    (if (= x 1) (car lista) (elem (- x 1) ( cdr lista ) ) )

)

(define (text-wh text font fontsize)
(let*
    (
        (text-width 1)
        (text-height 1)
    )
 
    (set! text-width (car (gimp-text-get-extents-fontname text fontsize PIXELS font)))    
    (set! text-height (elem 2  (gimp-text-get-extents-fontname text fontsize PIXELS font)))    
    
    (list text-width text-height)
    )
)

;(text-width "alma" "Sans" 100)

]]>
        </programlisting>
        <para>
            A következő részben létrehozzuk a képet az előbbi adatok alapján, és a króm hatás elérése érdekében színátmenettel fogunk dolgozni. Ezt követően a képet több lépésben rétegekből
            állítjuk össze.
        </para>
        <programlisting><![CDATA[

(define (script-fu-bhax-chrome text font fontsize width height color gradient)
(let*
    (
        (image (car (gimp-image-new width height 0)))
        (layer (car (gimp-layer-new image width height RGB-IMAGE "bg" 100 LAYER-MODE-NORMAL-LEGACY)))
        (textfs)
        (text-width (car (text-wh text font fontsize)))
        (text-height (elem 2 (text-wh text font fontsize)))
        (layer2)        
    )

]]>
        </programlisting>
        <para>
            Az első lépésben egy egyszerű fekete hátteret hozunk létre.
            Ez lesz a legalsó réteg, tulajdonképp a teljes képnek egy háttérként szolgál.
        </para>
        <programlisting><![CDATA[
;step 1
    (gimp-image-insert-layer image layer 0 0)
    (gimp-context-set-foreground '(0 0 0))
    (gimp-drawable-fill layer  FILL-FOREGROUND )
    (gimp-context-set-foreground '(255 255 255))
   
    (set! textfs (car (gimp-text-layer-new image text font fontsize PIXELS)))
    (gimp-image-insert-layer image textfs 0 0)   
    (gimp-layer-set-offsets textfs (- (/ width 2) (/ text-width 2)) (- (/ height 2) (/ text-height 2)))
   
    (set! layer (car(gimp-image-merge-down image textfs CLIP-TO-BOTTOM-LAYER)))
]]>
        </programlisting>
        <para>
            A következő lépésekben Gauss életlenítést alkalmazunk, beszúrunk egy köztes layert, és ismét életlenítünk. 
        </para>
        <programlisting><![CDATA[
;step 2   
    (plug-in-gauss-iir RUN-INTERACTIVE image layer 15 TRUE TRUE)
   
    ;step 3
    (gimp-drawable-levels layer HISTOGRAM-VALUE .11 .42 TRUE 1 0 1 TRUE)
   
    ;step 4   
    (plug-in-gauss-iir RUN-INTERACTIVE image layer 2 TRUE TRUE)

]]>
        </programlisting>
        <para>
            A kijelölés-invertálások és a térhatású effekt alkalmazás után megjelenítjük a kész képet.
        </para>
        <programlisting><![CDATA[
;step 5    
    (gimp-image-select-color image CHANNEL-OP-REPLACE layer '(0 0 0))
    (gimp-selection-invert image)

    ;step 6        
    (set! layer2 (car (gimp-layer-new image width height RGB-IMAGE "2" 100 LAYER-MODE-NORMAL-LEGACY)))
    (gimp-image-insert-layer image layer2 0 0)

    ;step 7        
    (gimp-context-set-gradient gradient) 
    (gimp-edit-blend layer2 BLEND-CUSTOM LAYER-MODE-NORMAL-LEGACY GRADIENT-LINEAR 100 0 REPEAT-NONE 
        FALSE TRUE 5 .1 TRUE width (/ height 3) width  (- height (/ height 3)))
    
    ;step 8        
    (plug-in-bump-map RUN-NONINTERACTIVE image layer2 layer 120 25 7 5 5 0 0 TRUE FALSE 2)
   
    ;step 9       
    (gimp-curves-spline layer2 HISTOGRAM-VALUE 8 (color-curve))
      
    (gimp-display-new image)
    (gimp-image-clean-all image)
    )
)
]]>
        </programlisting>
        <para>
            A forrás végén megadjuk a plugin nevét és leírását, valamint egy alapértelmezett kezdőértéket: Bátf41 Haxor lesz itt a króm
            szövegünk.
        </para>
        <programlisting><![CDATA[
;(script-fu-bhax-chrome "Bátf41 Haxor" "Sans" 120 1000 1000 '(255 0 0) "Crown molding")

(script-fu-register "script-fu-bhax-chrome"
    "Chrome3"
    "Creates a chrome effect on a given text."
    "Norbert Bátfai"
    "Copyright 2019, Norbert Bátfai"
    "January 19, 2019"
    ""
    SF-STRING       "Text"      "Bátf41 Haxor"
    SF-FONT         "Font"      "Sans"
    SF-ADJUSTMENT   "Font size" '(100 1 1000 1 10 0 1)
    SF-VALUE        "Width"     "1000"
    SF-VALUE        "Height"    "1000"
    SF-COLOR        "Color"     '(255 0 0)
    SF-GRADIENT     "Gradient"  "Crown molding"    
)
(script-fu-menu-register "script-fu-bhax-chrome" 
    "<Image>/File/Create/BHAX"
)

]]>
        </programlisting>

        <para>
        	<command>Mandala</command>
        </para>
        <para>
          <figure>
            <title>Név mandala</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/gimp_mandala.png"/>
                </imageobject>
                <textobject>
                    <phrase>Név mandala</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2019/01/10/a_gimp_lisp_hackelese_a_scheme_programozasi_nyelv">https://bhaxor.blog.hu/2019/01/10/a_gimp_lisp_hackelese_a_scheme_programozasi_nyelv</link>,
            Saját videó: <link xlink:href="https://youtu.be/dbsnleiJwzE">https://youtu.be/dbsnleiJwzE</link>      
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/GIMP_Lisp/Mandala">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/GIMP_Lisp/Mandala</link>               
        </para>
        <para>
            Ez a feladat sokban megegyezik az előzővel, de vannak különbségek.
        </para>         
        <programlisting><![CDATA[
(define (elem x lista)

    (if (= x 1) (car lista) (elem (- x 1) ( cdr lista ) ) )

)

(define (text-width text font fontsize)
(let*
    (
        (text-width 1)
    )
    (set! text-width (car (gimp-text-get-extents-fontname text fontsize PIXELS font)))    

    text-width
    )
)

(define (text-wh text font fontsize)
(let*
    (
        (text-width 1)
        (text-height 1)
    )
    ;;;
    (set! text-width (car (gimp-text-get-extents-fontname text fontsize PIXELS font)))    
    ;;; ved ki a lista 2. elemét
    (set! text-height (elem 2  (gimp-text-get-extents-fontname text fontsize PIXELS font)))    
    ;;;    
    
    (list text-width text-height)
    )
)


;(text-width "alma" "Sans" 100)
]]>
        </programlisting>
        <para>
            A mandalát a script-fu-bhax-mandala függvényben készítjük el.
            Beállítjuk a kép adatait, a szövegméretet, stb... 
        </para>
        <programlisting><![CDATA[
(define (script-fu-bhax-mandala text text2 font fontsize width height color gradient)
(let*
    (
        (image (car (gimp-image-new width height 0)))
        (layer (car (gimp-layer-new image width height RGB-IMAGE "bg" 100 LAYER-MODE-NORMAL-LEGACY)))
        (textfs)
        (text-layer)
        (text-width (text-width text font fontsize))
        ;;;
        (text2-width (car (text-wh text2 font fontsize)))
        (text2-height (elem 2 (text-wh text2 font fontsize)))
        ;;;
        (textfs-width)
        (textfs-height)
        (gradient-layer)
    )
]]>
        </programlisting>
        <para>
            Létrehozunk egy új layert, zöldre színezzük a hátteret,
            és a megadott szín szerint változtatjuk meg a foreground-ot is!
        </para>
        <programlisting><![CDATA[
(gimp-image-insert-layer image layer 0 0)

    (gimp-context-set-foreground '(0 255 0))
    (gimp-drawable-fill layer FILL-FOREGROUND)
    (gimp-image-undo-disable image) 

    (gimp-context-set-foreground color)

]]>
        </programlisting>
        <para>
            Létrehozunk egy szövegréteget (a kép méretei szerint).
            A következő szöveg rétegeket el fogjuk forgatni, és
            minden forgatott layert az előzőhöz kapcsolunk. A méreteket
            a textfs méreténénél 100 képponttal nagyobb állítjuk, és a
            réteget a képez méretezzük.
        </para>
        <programlisting><![CDATA[
(set! textfs (car (gimp-text-layer-new image text font fontsize PIXELS)))
    (gimp-image-insert-layer image textfs 0 -1)
    (gimp-layer-set-offsets textfs (- (/ width 2) (/ text-width 2))  (/ height 2))
    (gimp-layer-resize-to-image-size textfs)

    (set! text-layer (car (gimp-layer-new-from-drawable textfs image)))
    (gimp-image-insert-layer image text-layer 0 -1)
    (gimp-item-transform-rotate-simple text-layer ROTATE-180 TRUE 0 0)
    (set! textfs (car(gimp-image-merge-down image text-layer CLIP-TO-BOTTOM-LAYER)))

    (set! text-layer (car (gimp-layer-new-from-drawable textfs image)))
    (gimp-image-insert-layer image text-layer 0 -1)
    (gimp-item-transform-rotate text-layer (/ *pi* 2) TRUE 0 0)
    (set! textfs (car(gimp-image-merge-down image text-layer CLIP-TO-BOTTOM-LAYER)))

    (set! text-layer (car (gimp-layer-new-from-drawable textfs image)))
    (gimp-image-insert-layer image text-layer 0 -1)
    (gimp-item-transform-rotate text-layer (/ *pi* 4) TRUE 0 0)
    (set! textfs (car(gimp-image-merge-down image text-layer CLIP-TO-BOTTOM-LAYER)))
    
    (set! text-layer (car (gimp-layer-new-from-drawable textfs image)))
    (gimp-image-insert-layer image text-layer 0 -1)
    (gimp-item-transform-rotate text-layer (/ *pi* 6) TRUE 0 0)
    (set! textfs (car(gimp-image-merge-down image text-layer CLIP-TO-BOTTOM-LAYER)))    
    
    (plug-in-autocrop-layer RUN-NONINTERACTIVE image textfs)
    (set! textfs-width (+ (car(gimp-drawable-width textfs)) 100))
    (set! textfs-height (+ (car(gimp-drawable-height textfs)) 100))
        
    (gimp-layer-resize-to-image-size textfs)
]]>
        </programlisting>
        <para>
            Beállítjuk a brush méretét, és a gimp-edit-stroke utasítással
            elvégezzük a kijelölést a textfs layeren, amit ezután 70 képponttal kisebbre veszünk, és újra kijelölünk.
        </para>
        <programlisting><![CDATA[
(gimp-image-select-ellipse image CHANNEL-OP-REPLACE (- (- (/ width 2) (/ textfs-width 2)) 18) 
        (- (- (/ height 2) (/ textfs-height 2)) 18) (+ textfs-width 36) (+ textfs-height 36))
    (plug-in-sel2path RUN-NONINTERACTIVE image textfs)
        
    (gimp-context-set-brush-size 22)
    (gimp-edit-stroke textfs)
    
    (set! textfs-width (- textfs-width 70))
    (set! textfs-height (- textfs-height 70))
    
    (gimp-image-select-ellipse image CHANNEL-OP-REPLACE (- (- (/ width 2) (/ textfs-width 2)) 18) 
        (- (- (/ height 2) (/ textfs-height 2)) 18) (+ textfs-width 36) (+ textfs-height 36))
    (plug-in-sel2path RUN-NONINTERACTIVE image textfs)

    (gimp-context-set-brush-size 8)
    (gimp-edit-stroke textfs)
]]>
        </programlisting>
        <para>
            Alkalmazzuk a színátmenetet. Létrehuznk egy második szöveg réteget, ez lesz a mandala közepén. Az elkészült képet megjelenítjük. A kommentelt résszel a kijelölést is megszüntethetnénk.
        </para>
        <programlisting><![CDATA[
(set! gradient-layer (car (gimp-layer-new image width height RGB-IMAGE "gradient" 100 LAYER-MODE-NORMAL-LEGACY)))
    
    (gimp-image-insert-layer image gradient-layer 0 -1)
    (gimp-image-select-item image CHANNEL-OP-REPLACE textfs)
    (gimp-context-set-gradient gradient) 
    (gimp-edit-blend gradient-layer BLEND-CUSTOM LAYER-MODE-NORMAL-LEGACY GRADIENT-RADIAL 100 0 
    REPEAT-TRIANGULAR FALSE TRUE 5 .1 TRUE (/ width 2) (/ height 2) (+ (+ (/ width 2) (/ textfs-width 2)) 8) (/ height 2))
    
    (plug-in-sel2path RUN-NONINTERACTIVE image textfs)

    (set! textfs (car (gimp-text-layer-new image text2 font fontsize PIXELS)))
    (gimp-image-insert-layer image textfs 0 -1)
    (gimp-message (number->string text2-height))
    (gimp-layer-set-offsets textfs (- (/ width 2) (/ text2-width 2)) (- (/ height 2) (/ text2-height 2)))
        
    ;(gimp-selection-none image)
    ;(gimp-image-flatten image)
    
    (gimp-display-new image)
    (gimp-image-clean-all image)
    )
)

;(script-fu-bhax-mandala "Bátfai Norbert" "BHAX" "Ruge Boogie" 120 1920 1080 '(255 0 0) "Shadows 3")

]]>
        </programlisting>
        <para>
            Végül (ahogy az előző feladatban is) hozzáadjuk a menühöz
            a modot, készítünk hozzá egy rövid leírást ("regisztráljuk a programban"), és beállítjuk a kezdőértékeket (forgatandó szöveg, középső szöveg, betűtípus, betűméret, képméret, szín, átmenet).
        </para>
        <para>
            A programban használt utasítások megtalálhatók a gimp dokumentációban, pl:
        </para>
        <para>
          <figure>
            <title>Kijelölés</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/gimp_stroke_doc.png"/>
                </imageobject>
                <textobject>
                    <phrase>Kijelölés</phrase>
                </textobject>
            </mediaobject>
          </figure>
        </para>
    </section>

</chapter>